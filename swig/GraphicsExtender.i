%include "std_string.i"
%include "enumtypeunsafe.swg"

%javaconst(1);

%module GraphicsExtender
%{
#include <il2ge_java_interface/interface.h>
%}

%pragma(java) jniclassclassmodifiers="class"

%include il2ge_java_interface/interface.h
%include module_code.i
%include jniclass_code.i
