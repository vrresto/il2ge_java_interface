cmake_minimum_required(VERSION 3.13)

project(il2ge_java_interface)

include(interface_version.cmake)

include_directories(
  ${CMAKE_CURRENT_BINARY_DIR}
)

add_subdirectory(swig)
